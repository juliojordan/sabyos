(function(app) {
  app.AppComponent =
    ng.core.Component({
      selector: 'events',
      templateUrl: 'src/templates/events.html'
    })
    .Class({
      constructor: function() {
        this.events = [
          { name: 'Madrid Centro', location: 'Café Barbieri', date: '3 de Febrero', url: 'img/paisano0.jpg'},
          { name: 'Madrid Centro', location: 'Café Barbieri', date: '3 de Febrero', url: 'img/paisano1.jpg'},
          { name: 'Historia del Barrio', location: 'Calle del León 13', date: '12 de Enero', url: 'img/paisano0.jpg' },
        ];
        console.log(this.events);
      }

    });

  app.EventComponent =
    ng.core.Component({
      selector: 'event',
      templateUrl: 'src/templates/event.html',
      inputs: ['url', 'name', 'date', 'location']
    })
    .Class({
      constructor: function() {
        this.isPastEvent = function(){

          return typeof this.url !== 'undefined' ;
        };
      }
    });

  app.VideoHeaderComponent =
  ng.core.Component({
    selector: 'videosabyo',
    templateUrl: 'src/templates/video.html',
  })
  .Class({
    constructor: function() {
      (function () {
          var video = document.getElementById("video");
          if (video) {
            video.volume = 0;
          }
      })();
    }
  });

  app.NavbarComponent =
  ng.core.Component({
    selector: 'navbarsabyo',
    templateUrl: 'src/templates/nav-bar.html',
  })
  .Class({
    constructor: function() {
    }
  });

})(window.app || (window.app = {}));
