(function(app) {
  app.AppModule =
    ng.core.NgModule({
      imports: [ ng.platformBrowser.BrowserModule ],
      declarations: [ app.AppComponent, app.EventComponent, app.VideoHeaderComponent, app.NavbarComponent ],
      bootstrap: [ app.AppComponent, app.EventComponent,  app.VideoHeaderComponent, app.NavbarComponent ]
    })
    .Class({
      constructor: function() {}
    });
})(window.app || (window.app = {}));
