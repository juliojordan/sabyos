const express = require("express");
const app = express();
const PORT = process.env.PORT || 3000;

app.use(express.static(__dirname + '/'));

// GET /
app.get('/', function (req, res) {
    res.sendFile("index.html");
});

// GET /experts
app.get('/experts', function (req, res) {
    res.sendFile("experts.html");
});

// GET /helpers
app.get('/helpers', function (req, res) {
    res.sendFile("helpers.html");
});

app.listen(PORT, function () {
    console.log(`the audience is listening on ${PORT}`);
});